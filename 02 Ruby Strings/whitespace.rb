name = 'David'

# Special characteres that represent whitespace
string = "Hello, my name is #{name}. Workspaces is fun!"
puts string

# New line
string = "Hello.\nMy name is #{name}.\nWorkspaces is fun!"
puts string

# ...and tabs
string = "Hello.\n\tMy name is #{name}.\n\tWorkspaces is fun!"
puts string

# ...and spaces
string = "Hello.\sMy\sname\sis\s#{name}.\sWorkspaces\sis\sfun!"
puts string
