name = 'David Rojo'

puts name
puts name.upcase
puts name.downcase

# Methods usually don't modify data they return new versions of it.
reversed_name = name.reverse
puts reversed_name
puts name

# As the values returnes by methods are of the same type, methods can be chained
puts name.upcase.reverse

# Bang versions of methods DO modify the data in the variable.
name.upcase!
puts name
