name = 'David'

# Single quoted strings doesn't allow interpolation
puts 'Hello #{name}!'

# Double quoted strings does allow it
puts "Hello #{name}!"

# There are other ways of declare strings
puts %Q{Hello #{name}!}
puts %Q|Hello #{name}!|

# Strings can split over several rows
string = <<-HELLO
Hello
My name is #{name}
Workspaces is fun!
HELLO

puts string
