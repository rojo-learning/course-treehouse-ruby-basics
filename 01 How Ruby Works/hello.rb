# Printing some text
puts 'Hello World!'

# Optional parenthesis and semicolons
puts('Hello World!');

# Hold data into variables
name = 'David'

puts 'Hello'
puts name
