# Arguments are defined between parenthesis
def add(a, b)
  puts "Adding #{a} and #{b}!"
  a + b
end

# Then, when methods are called, they are feed with values
puts add(4, 2)

# They are very useful if the instructions are required several times
puts add(3, 3)
puts add(5, 1)
