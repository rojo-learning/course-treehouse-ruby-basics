# The `return` keyword sets the value to return and exits the method
def add(a, b)
  puts "Adding #{a} and #{b}."
  return a + b
  puts "This line doesn't gets executed"
end

puts add(1,2)
puts add(3,4)
puts add(5,6)
