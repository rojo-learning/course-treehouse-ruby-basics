# `puts` prints to output but returns nil
puts 'Hello' # => nil

# operators are methods of object numbers
puts 2.+4

# returns the addition of two numbers
def add
  2 + 4
end

puts add
