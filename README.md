# Treehouse - Ruby Basics #

This repository contains notes and practice examples from **Ruby Basics**, imparted by Jason Seifer at Threehouse.

> Ruby is a programming language with a focus on simplicity and productivity. It has an elegant syntax that is natural to read and easy to write. In Ruby Basics, we'll learn how to work with Ruby and write simple Ruby programs. You can take this course right in the browser -- we'll be using Workspaces to jump right in to the code!

## Contents ##

- **How Ruby Works**: Ruby is a simple but powerful programming language that can be used to make all kinds of programs. In "How Ruby Works," we’ll learn the rules for creating ruby programs and then practice those rules and run our programs.
- **Ruby Strings**: Strings are a basic Ruby type that allow us to work with text. In this stage, we'll learn all about how to create and work with strings in Ruby.
- **Ruby Numbers**: Numbers are basic type in any programming languages. In the Ruby Numbers badge, we'll start working with numbers and use IRB as a simple calculator.
- **Ruby Methods**: Methods, sometimes called functions, are a named series of statements that perform a specific task. In this stage, we'll learn how to create and work with our own methods.

---
This repository contains code examples from Treehouse. These are included under fair use for showcasing purposes only. Those examples may have been modified to fit my particular coding style.
